package com.lu.lumall.lumallware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumallWareApplication.class, args);
    }

}
