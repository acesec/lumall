package com.lu.lumall.lumallcoupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumallCouponApplication.class, args);
    }

}
