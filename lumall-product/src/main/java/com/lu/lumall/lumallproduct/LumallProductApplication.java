package com.lu.lumall.lumallproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumallProductApplication.class, args);
    }

}
