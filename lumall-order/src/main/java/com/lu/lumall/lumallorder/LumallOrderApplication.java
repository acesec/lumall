package com.lu.lumall.lumallorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumallOrderApplication.class, args);
    }

}
