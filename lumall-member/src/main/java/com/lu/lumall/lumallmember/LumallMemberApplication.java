package com.lu.lumall.lumallmember;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LumallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(LumallMemberApplication.class, args);
    }

}
